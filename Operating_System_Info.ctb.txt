# Operating System Info
Website : 			https://www.howtogeek.com/361572/what-is-an-operating-system/ 
					
					https://www.youtube.com/watch?v=vBURTt97EkA  
					
					https://en.wikipedia.org/wiki/OpenSUSE
					
					https://www.opensuse.org/ 
					
					https://en.wikipedia.org/wiki/Arch_Linux   
					
					https://archlinux.org/
					
					https://wiki.archlinux.org/
						

What is an Operating System (OS) ?  

An  operating system is the primary software that manages all the hardware and the other software on a computer. 
The  operation system, also known as an ‘OS’ interfaces with the computer's hardware and provides services that  applications can use. 

Opensuse Linux Distribution 

According to wikipedia openSUSE is well known  for its Linux distributions, mainly Tumbleweed , a testing rolling realease, and Leap , 
a distribution with long-term-support . 

MicroOS and Kubic are new transactional, self-contained distributions for use as desktop or container runtime.
The initial release of the community project was a beta version of SUSE Linux 10.0. The current stable fixed release is openSUSE Leap 15.3.
Additionally the project creates a variety of tools, such as YaST, Open Build Service, openQA, Snapper, Machinery, Portus, KIWI and OSEM, working together in an open, transparent and friendly manner as part of the worldwide Free and Open Source Software community.


Arch Linux Distribution 


According to Wikipedia arch linux is a linux distribution meant for computers with  x86-64 processors. 
Arch Linux adheres to the KISS principle  (" Keep It Simple , Stupid ").  The project attempts to have  minimal 
distribution -specific 	changes ,  and therefore  minimal breakage with updates, and be pragmatic over ideological design choices and focus on customizability rather than user-friendliness. 



## How to Downgrade a package (software)
Website : 		 https://wiki.archlinux.org/title/Arch_Linux_Archive 

				 https://wiki.archlinux.org/title/Downgrading_packages  
				
				https://www.ubuntupit.com/how-to-downgrade-packages-on-linux-system-the-ultimate-guide/  
				
				https://www.maketecheasier.com/downgrade-software-package-linux/
				
				https://software.opensuse.org/package/fontawesome-fonts  
				
				https://forums.opensuse.org/showthread.php/547415-How-to-downgrade-revert-a-package  
			

Arch Linux 			

How to downgrade one package(software) ? 

Method 1 : 


Find the package you want under /packages and let pacman fetch it for installation. For example:

# pacman -U https://archive.archlinux.org/packages/path/packagename.pkg.tar.xz 	

Example : 

How to downgrade fontawesome to 5.14.0-1 version 

$ sudo pacman -U https://archive.archlinux.org/packages/t/ttf-font-awesome-5.14.0-1-any.pkg.tar.zst  


How to downgrade rofi to 1.6.1-1 version 

$ sudo pacman -U https://archive.archlinux.org/packages/r/rofi-1.6.1-1-x86_64.pkg.tar.zst 



Method 2 : 

If you wished to downgrade a package , first you need to find the package in your root directory . 

Then type  (ls) = list  command in the terminal or command line . 

$ ls /var/cache/pacman/pkg/ | grep package_name

$ sudo pacman -U /var/cache/pacman/pkg/package_name-version.pkg.tar.xz  


OpenSUSE Tumbleweed 

How to downgrade one package(software) ?   

When downgrade a package in openSUSE Tumbleweed , 
in my experience you have to search for  the Archive repository  and download  it from there . 


 1.  Step 1 : You vist this website http://download.opensuse.org/history/20210828/tumbleweed/repo/oss/noarch/  
 					and search for the package name you wished to download 
 
 2.  Step 2 : Click on the package name you wished to download and you have the option to download the package with either 
 					Yast 2 or command line . 

 3.  Step 3 : Select the option to save the file . 
 
  
 
 4. Step 4 : Ensure to save the file in your download folder .  
 
 
 
 5. Step 5 : The filename is “fontawesome-fonts-5.14.0-1.4.noarch.rpm ” you are going to install it in your system now . 
 					Using the command 
 					
 					“ sudo rpm  -ivh  filename.rpm”   
 
 					The “ i ” represents install 
 					
 					The “ v ” represents verbose 
 
 					The “  h ” represents hash 
 				
 
 
 6. Step 6 : And now you have successful downgrade a package on your system . 
 				To check which version you are currently running type 
 
 				“  rpm -qa | grep  fontawesome-fonts  ”
  

### OpenSUSE_Setup
# This is to help setup opensuse when you first boot up . 

## Reference :   

						 https://fosspost.org/things-to-do-after-installing-opensuse/ 

 						 https://www.tecmint.com/things-to-do-after-installing-opensuse-leap-15/ 

  						 https://ostechnix.com/how-to-install-multimedia-codecs-in-opensuse/ 
  
						  https://en.opensuse.org/SDB:System_upgrade 
						
						https://www.xmodulo.com/add-packman-repository-opensuse.html 



## System Update first 

	    : sudo zypper refresh  
      
	    : sudo zypper dup 

## First enable the packman respository 

## Enable the media codes 

## Install AMD Drivers or NVDIA Drivers if present on your system   
